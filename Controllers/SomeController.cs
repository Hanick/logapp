﻿using Microsoft.AspNetCore.Mvc;

namespace LogApp.Controllers;

[Route("api/[controller]")]
[ApiController]
public class SomeController : ControllerBase
{
    private readonly ILogger<SomeController> _logger;

    public SomeController(ILogger<SomeController> logger)
    {
        _logger = logger;
    }

    [HttpGet]
    public ActionResult<IEnumerable<string>> Get()
    {
        var names = new string[] { "foo", "bar" };
        _logger.LogInformation("Getting names", names);
        return names;
    }

    [HttpGet("{id}")]
    public ActionResult<string> Get(int id)
    {
        return "name";
    }

    [HttpPost]
    public void Post([FromBody] string value)
    {
    }

    [HttpPut("{id}")]
    public void Put(int id, [FromBody] string value)
    {
    }

    [HttpDelete("{id}")]
    public void Delete(int id)
    {
    }
}
